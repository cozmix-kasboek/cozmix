const colors = require('tailwindcss/colors')

module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  theme: {
    colors: {
      orange: colors.orange,
      gray: colors.gray,
      white: colors.white,
      green: colors.green,
      red: colors.red
    },
    minWidth: {
      'full': '95%'
    }
  },
  variants: {},
  plugins: [],
}
