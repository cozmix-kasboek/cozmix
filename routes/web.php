<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'web'], function() {
    Route::group(['middleware' => ['auth:sanctum']], function() {
        Route::get('/',function() {
            return view('overview');
        });
    
        Route::get('/producten', 'ProductController@index')->name('products.index');
        Route::delete('/producten/{id}', 'ProductController@destroy');
        Route::put('/producten/{id}', 'ProductController@edit');
        Route::get('/producten/{id}', 'ProductController@form')->where('id', '[0-9]+')->name('products.form');
        Route::get('/producten/toevoegen', 'ProductController@newform')->name('products.newform');
        Route::post('/producten/toevoegen', 'ProductController@create');
    
        Route::get('/categorieen', 'CategoryController@index')->name('categories.index');
        Route::get('/categorieen/{id}', 'CategoryController@form')->where('id', '[0-9]+')->name('categories.form');
        Route::get('/categorieen/toevoegen', 'CategoryController@newform')->name('categories.newform');
        Route::post('/categorieen/toevoegen', 'CategoryController@create');
        Route::delete('/categorieen/{id}', 'CategoryController@destroy');
        Route::put('/categorieen/{id}', 'CategoryController@edit');
    
        Route::get('/codes', 'CodeController@index')->name('codes.index');
        Route::get('/codes/{id}', 'CodeController@form')->where('id', '[0-9]+')->name('codes.form');
        Route::get('/codes/toevoegen', 'CodeController@newform')->name('codes.newform');
        Route::post('/codes/toevoegen', 'CodeController@create');
        Route::delete('/codes/{id}', 'CodeController@destroy');
        Route::put('/codes/{id}', 'CodeController@edit');
    
        Route::get('/rollen', 'RoleController@index')->name('roles.index');
        Route::get('/rollen/{id}', 'RoleController@form')->where('id', '[0-9]+')->name('roles.form');
        Route::get('/rollen/toevoegen', 'RoleController@newform')->name('roles.newform');
        Route::post('/rollen/toevoegen', 'RoleController@create');
        Route::delete('/rollen/{id}', 'RoleController@destroy');
        Route::put('/rollen/{id}', 'RoleController@edit');
    
        Route::get('/gebruikers', 'PersonController@index')->name('persons.index');
        Route::get('/gebruikers/{id}', 'PersonController@form')->where('id', '[0-9]+')->name('persons.form');
        Route::get('/gebruikers/toevoegen', 'PersonController@newform')->name('persons.newform');
        Route::post('/gebruikers/toevoegen', 'PersonController@create');
        Route::delete('/gebruikers/{id}', 'PersonController@destroy');
        Route::put('/gebruikers/{id}', 'PersonController@edit');
    
    
        Route::get('/transacties', 'TransactionController@index')->name('transactions.index');
        Route::get('/transacties/{id}', 'TransactionController@form')->where('id', '[0-9]+')->name('transactions.form');
        Route::get('/transacties/toevoegen', 'TransactionController@newform')->name('transactions.newform');
        Route::post('/transacties/toevoegen', 'TransactionController@create');
        Route::delete('/transacties/{id}', 'TransactionController@destroy');
        Route::put('/transacties/{id}', 'TransactionController@edit');
    
        Route::get('/transactietypes', 'TransactionTypeController@index')->name('types.index');
        Route::get('/transactietypes/{id}', 'TransactionTypeController@form')->where('id', '[0-9]+')->name('types.form');
        Route::get('/transactietypes/toevoegen', 'TransactionTypeController@newform')->name('types.newform');
        Route::post('/transactietypes/toevoegen', 'TransactionTypeController@create');
        Route::delete('/transactietypes/{id}', 'TransactionTypeController@destroy');
        Route::put('/transactietypes/{id}', 'TransactionTypeController@edit');
    
        Route::get('/betaalmethodes', 'PaymentMethodController@index')->name('paymentmethods.index');
        Route::get('/betaalmethodes/{id}', 'PaymentMethodController@form')->where('id', '[0-9]+')->name('paymentmethods.form');
        Route::get('/betaalmethodes/toevoegen', 'PaymentMethodController@newform')->name('paymentmethods.newform');
        Route::post('/betaalmethodes/toevoegen', 'PaymentMethodController@create');
        Route::delete('/betaalmethodes/{id}', 'PaymentMethodController@destroy');
        Route::put('/betaalmethodes/{id}', 'PaymentMethodController@edit');
    
        Route::get('/gebeurtenissen', 'EventController@index')->name('events.index');
        Route::get('/gebeurtenissen/toevoegen', 'EventController@newform')->name('events.newform');
        Route::get('/gebeurtenissen/{id}', 'EventController@form')->name('events.form');
        Route::post('/gebeurtenissen/toevoegen', 'EventController@create');
        Route::delete('/gebeurtenissen/{id}', 'EventController@destroy');
        Route::put('/gebeurtenissen/{id}', 'EventController@edit');
    
        Route::get('/exports', 'ExportController@index');
        Route::post('/export', 'ExportController@export');
        Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
            return view('overview');
        })->name('dashboard');
    
    });
    
});
