<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function () {
    // to insert into group::(namespace) when app is finalized, migrations have been run, token is set in app and in transactionsform view
});

Route::group(['namespace' => 'api'], function() {

        Route::get('users', 'UserController@index');
        Route::get('users/{id}', 'UserController@show'); // not used, for future development

        Route::get('roles', 'RoleController@index');
        Route::get('roles/{id}', 'RoleController@show'); // not used, for future development
        Route::post('roles/{role}/login', 'RoleController@login');

        Route::get('products', 'ProductController@index');
        Route::get('products/{id}', 'ProductController@show'); // not used, for future development

        Route::get('transactions', 'TransactionController@index'); // not used, for future development
        Route::get('transactions/{id}', 'TransactionController@show'); // not used, for future development
        Route::post('transactions', 'TransactionController@store');
        Route::post('transactions/price', 'TransactionController@price');

        Route::get('codes', 'CodeController@index'); // not used, for future development
        Route::get('codes/{id}', 'CodeController@show'); // not used, for future development

        Route::get('paymentmethods', 'PaymentMethodController@index'); // not used, for future development
        Route::get('paymentmethods/{id}', 'PaymentMethodController@show'); // not used, for future development

        Route::get('types', 'TypeController@index'); // not used, for future development
        Route::get('types/{id}', 'TypeController@show'); // not used, for future development

        Route::get('categories', 'CategoryController@index'); // not used, for future development
        Route::get('categories/{id}', 'CategoryController@show'); // not used, for future development

        Route::get('events', 'EventController@index'); // not used, for future development
        Route::get('events/{id}', 'EventController@show'); // not used, for future development
        Route::post('events', 'EventController@store');

});

Route::post('/sanctum/token', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    if (! $user || ! Hash::check($request->password, $user->password)) {
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.'],
        ]);
    }

    return $user->createToken($request->device_name)->plainTextToken;
});