# Cozmix kasboek beheerplatform

Het platform dient om de data die gebruikt wordt in de Android-applicatie te beheren, alsook gegevens op te vragen van transacties aangemaakt in de app.

## Set-up (algemeen)

1. Database aanmaken (lokaal of online) en authenticatiegegevens database in .env aanvullen
- DB_HOST
- DB_PORT
- DB_DATABASE
- DB_USERNAME
- DB_PASSWORD

```bash
cp .env.example .env
```

2. Migration en seeders starten om databasestructuur aan te maken en (mock)gegevens in de database te stoppen

```bash
php artisan migrate
php artisan db:seed
```

## Set-up (lokaal)

Het framework Laravel werd gebruikt om het volledige platform te bouwen. Er zijn verschillende manieren om de applicatie lokaal te draaien, maar in ons geval gebruikten we Homestead ([link naar de documentatie](https://laravel.com/docs/8.x/homestead)).

## Set-up (online hosting)

Zowel database als de webinterface kan gehost op bijvoorbeeld Combell.

1. Clonen van project (git clone ...)
2. Stappen van Set-up (algemeen) volgen

## Gebruik

Het platform is bereikbaar op [https://cozmix.lauwaet.be](https://cozmix.lauwaet.be).

Tijdelijke inloggegevens
- E-mailadres: **demo@cozmix.be**
- Wachtwoord: **Cozmixdemo123**

Een nieuwe beheerplatform kan aangemaakt worden op de url **https://cozmix.lauwaet.be/register**.

## Functionaliteiten

### Gegevensbeheer

Onderstaande data kan beheerd worden via het platform:
- Gebruikers
- Gebruikersrollen
- Producten
- Productcategorieën
- Boekhoudkundige codes
- Transacties
- Transactietypes
- Betaalmethodes
- Gebeurtenissen

### Exporteren van geregistreerde transacties

Door middel van een filter kunnen de transacties over een bepaalde periode geëxporteerd worden naar een Excelbestand.
De huidige output is de volgende:
Aan één transactie kunnen meerdere producten gekoppeld zijn (bijvoorbeeld twee tickettypes). Die worden elk op een nieuwe lijn weergegeven met een lijntotaal en een gekoppelde boekhoudkundige code.

De wens was echter om een geaggregeerde output te hebben (geen onderverdeling van welke producten de transactie bevat), maar wel met een gekoppelde boekhoudkundige code in de output. In ons datamodel hebben we ervoor gekozen om een code te koppelen aan een product en niet aan een transactie. Om de gewenste output te bekomen, werd volgende query geschreven [link naar bestand](...TODO...)

### API en synchronisatie

Om te communiceren met de mobiele applicatie werd een API ontwikkeld.
De volgende data kan via een CRUD-operatie gewijzigd worden waarna die automatisch gesynchoniseerd wordt met de mobiele applicatie. 
- Gebruikers (met een gekoppelde gebruikersrol)
- Producten (met een gekoppelde productcategorie en boekhoudkundige code)

Anderzijds wordt volgende data van de mobiele applicatie naar het beheerplatform gestuurd:
- Gebeurtenissen
- Transacties

#### API-beveiliging

Indien gewenst kan de API beveiligd worden met een key door de volgende stappen uit te voeren: 

1. Stuur een POST-request (Postman) naar /api/sanctum/token met de volgende body (e-mail en wachtwoord van het beheerplatformaccount en een eigen gekozen naam voor het toestel)

```bash
{
    "email" : "demo@cozmix.be",
    "password" : "Cozmixdemo123",
    "device_name" : "tablet"
}

```
2. Als antwoord op bovenstaande request krijg je een token terug die in de headers van een request moet meegegeven worden in de code van de mobiele applicatie.
3. Zet alle routes in de beveiligde Sanctum-groep in het bestand [routes/api.php](routes/api.php)

Documentatie:

[Laravel Sanctum](https://laravel.com/docs/8.x/sanctum#issuing-mobile-api-tokens)


## Aandachtspunten

### Synchronisatie bij wijzigingen op beheerplatform

Er is een volledige implementatie voorzien van alle CRUD-operaties (aanmaken, lezen, updaten en verwijderen van gegevens), maar de synchronisatie richting de applicatie wordt niet doorgevoerd bij volgende 'entiteiten' wanneer een CRUD-operatie gebeurt.

- Betaalmethodes
- Productcategorieën
- Transactietypes


## Verdere ontwikkeling

### Uploadmogelijkheid foto's

De initiële scope voorzag om ook foto's van gebruikers en producten te kunnen uploaden, maar omwille van een wijziging van de prioriteiten werd deze feature niet geïmplementeerd. 