<?php 
namespace App;
use Illuminate\Http\Request;
use App\Models\ProductTransaction;
use App\Models\Product;
use PhpParser\Node\Stmt\Foreach_;
use Ramsey\Uuid\Uuid;

class CustomFunctions {

public function __construct()
{

}

public function makePersonFromValidatedData($person, $validated){
    $person->first_name = $validated['first_name'];
    $person->last_name = $validated['last_name'];
    $person->role_id = $validated['role_id'];
    $person->img_src = $validated['first_name'].'_'.$validated['last_name'].'.jpg';
    return $person;
}

public function makeProductFromValidatedData($product, $validated){
    $product->name = $validated['name'];

    if (isset($validated['price'])){
        $product->price = $validated['price'];
    }

    if (isset($validated['description'])){
        $product->description = $validated['description'];
    };
 
    $product->code_id = $validated['code_id'];
    $product->category_id = $validated['category_id'];

    return $product;
}

public function makeObjectFromValidatedData($object, $validated){

    $object->name = $validated['name'];

    if (isset($validated['description'])){
        $object->description = $validated['description'];
    };
 
    return $object;
}


public function makeEventFromValidatedData($event, $validated){

    if (!isset($event->id)){
        $event->id = Uuid::uuid4()->toString();
    }
    
    $event->name = $validated['name'];
    $event->datetime = $validated['datetime'];

    return $event;
}

public function makeApiEventFromValidatedData($event, $validated){
    
    $event->id = $validated['id'];
    $event->name = $validated['name'];
    $event->datetime = $validated['datetime'];

    return $event;
}

public function makeRoleFromValidatedData($role, $validated){
    $role->name = $validated['name'];
    $role->description = $validated['description'];
    $role->pin = $validated['pin'];

    return $role;
}

public function makeTransactionFromValidatedData($transaction, $validated){
    $transaction->date = $validated['date'];
    if (isset($validated['description'])){
        $transaction->description = $validated['description'];
    }
    $transaction->transaction_amount = $validated['transaction_amount'];

    /* When updating the information of a transaction, the linked event could eventually be reset. This is a check to update the information accordingly.*/
    if ($transaction->event_id != null && !isset($validated['event_id'])){
        $transaction->event_id = null;
    }

    if (isset($validated['event_id'])){
        $transaction->event_id = $validated['event_id'];
    }
    $transaction->person_id = $validated['person_id'];
    $transaction->type_id = $validated['type_id'];

    /* When updating the information of a transaction, the payment method could eventually be reset. This is a check to update the information accordingly.*/
    if ($transaction->payment_method_id != null && !isset($validated['payment_method_id'])){
        $transaction->payment_method_id = null;
    }
    
    if (isset($validated['payment_method_id'])){
        $transaction->payment_method_id = $validated['payment_method_id'];
    }

    $transaction->save();

    $productTransactions = ProductTransaction::where('transaction_id', $transaction->id)->get();


    foreach ($productTransactions as $productTransaction){
        $productTransaction->delete();
    }

    if(isset($validated['products'])){
        for ($i = 0; $i < count($validated['products']); $i++){

            $productTransaction = ProductTransaction::where(['transaction_id' => $transaction->id, 'product_id' => $validated['products'][$i]])->first();
            if ($productTransaction == null){
                $productTransaction = new ProductTransaction;            
                $productTransaction->product_id = $validated['products'][$i];
                $productTransaction->transaction_id = $transaction->id;
                $productTransaction->quantity = $validated['quantities'][$i]; 
            }
            else {
    
                $productTransaction->quantity = $productTransaction->quantity + $validated['quantities'][$i]; 
            } 
            $productTransaction->save();
        }    
    }


    return $transaction;

}

public function makeApiTransactionFromValidatedData($transaction, $validated){

    $price = NULL;



    if ($validated['transaction']['transaction_amount'] > 0) {
        $price = $validated['transaction']['transaction_amount'];
    }

    else if (array_key_exists('products', $validated) && $validated['products'] != null ) {
        $price = $this->calculatePrice($validated['products']);
    }

    $transaction->date = $validated['transaction']['date'];
    $transaction->description = $validated['transaction']['description'];
    $transaction->transaction_amount = $price;

    if ($validated['transaction']['event_id'] === "") {
        $transaction->event_id = null;
    } else {
        $transaction->event_id = $validated['transaction']['event_id'];
    }
    
    $transaction->person_id = $validated['transaction']['user_id'];
    $transaction->type_id = $validated['transaction']['type_id'];
    $transaction->payment_method_id = $validated['transaction']['payment_method_id'];

    $transaction->save();
        
    $transaction_id = $transaction->id;

    if (array_key_exists('products', $validated) && $validated['products'] != null ) {
        $products = $validated['products'];

        foreach($products as $product) 
        {
            $pt = new ProductTransaction;
            $product_id = $product['productId'];
            $quantity = $product['quantity'];
            $pt->transaction_id = $transaction_id;
            $pt->product_id = $product_id;
            $pt->quantity = $quantity;

            $pt -> save();
        }
    }
 
    return $transaction;

}

public function makeEventsFromValidatedData($validated){
    
}

public function calculatePrice($products){
    $price = 0;
    foreach($products as $product){
        $productType = Product::find($product['productId']);
        $price = $price + ($productType->price * $product['quantity']);
    }

    return $price;

}

}