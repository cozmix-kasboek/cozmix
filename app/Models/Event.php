<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Event extends Model
{
    use Sortable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $keyType = 'string';
    public $incrementing = false;


    protected $fillable = [
        'id',
        'name',
        'date',
        'time'
    ];

    public $sortable = [
        'id',
        'name',
        'datetime'
    ];

    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction')
                        ->with('products');
    }

}
