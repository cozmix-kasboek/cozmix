<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Person extends Model
{
    use Sortable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'first_name',
        'last_name',
        'img_src',
        'role_id',
    ];

    public $sortable = [
        'id',
        'first_name',
        'last_name'
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction');
    }
}
