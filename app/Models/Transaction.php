<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Transaction extends Model
{
    use Sortable;
    public $timestamps = false;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'date',
        'time',
        'description',
        'transaction_amount',
        'person_id',
        'type_id',
        'event_id',
        'payment_method_id'
    ];

    public $sortable = [
        'id',
        'date',
        'time',
        'description',
        'transaction_amount'
    ];
    

    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'event_id');
    }

    public function person()
    {
        return $this->belongsTo('App\Models\Person', 'person_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type', 'type_id');
    }

    public function paymentmethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','products_transactions')
                        ->withPivot('quantity');

    }
}
