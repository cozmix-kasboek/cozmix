<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ProductTransaction extends Model
{
    use Sortable;
    public $timestamps = false;

    protected $primaryKey = 'transaction_id';
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products_transactions';

    protected $fillable = [
        'transaction_id',
        'product_id',
        'quantity'
    ];
}
