<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    use Sortable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'price',
        'description',
        'code_id',
        'category_id'
    ];

    public $sortable = [
        'id',
        'name',
        'price',
        'description',
        'code_id',
        'category_id'
    ];

    public function transactions()
    {
        return $this->belongsToMany('App\Models\Transaction', 'id')
            ->using('App\Models\ProductTransaction');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id');
    }

    public function code()
    {
        return $this->belongsTo('App\Models\Code', 'code_id');
    }
}
