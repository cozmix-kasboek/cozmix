<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Role extends Model
{
    use Sortable;
    public $timestamps = false;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'description'
    ];

    public $sortable = [
        'id',
        'name',
        'description',
        'pin'
    ];
    

    public function person(){
        return $this->hasMany('App\Models\Person');
    }
}
