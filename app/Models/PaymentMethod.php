<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PaymentMethod extends Model
{
    use Sortable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'description'
    ];

    public $sortable = [
        'id',
        'name',
        'description'
    ];

    public function transaction(){
        return $this->hasMany('App\Models\Transaction');
    }
}
