<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Exports\TransactionsByDateExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ExportController extends Controller
{
    public function index(){
        return view('exports');
    }
    
    public function export(Request $request)
    {

        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $file_name = $from_date.'_to_'.$to_date.'_export.xlsx';
        return Excel::download(new TransactionsByDateExport($from_date, $to_date), $file_name);

    }
}
