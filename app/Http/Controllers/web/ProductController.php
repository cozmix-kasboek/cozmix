<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Code;
use App\Models\ProductCategory;
use App\Http\Requests\ProductStoreRequest;
use App\CustomFunctions;

class ProductController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Product $product){
        $products = $product->sortable()->paginate(10);                        
        return view('products')->with('products', $products);
    }

    public function destroy($id){
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('products.index')->with('success', 'Product verwijderd');
    }

    public function form($id){
        
        $product = Product::find($id);
        $selectedCode = $product->code_id;
        $selectedCategory = $product->category_id;

        $codes = Code::all();
        $categories = ProductCategory::all();
        return view('forms.productsform',['product' => $product, 'codes' => $codes, 'selectedCode' => $selectedCode, 'categories' => $categories, 'selectedCategory' => $selectedCategory ]);
    }

    public function newform(){ 
        $product = new Product;  
        $codes = Code::all();
        $categories = ProductCategory::all();     
        return view('forms.productsform', ['product' => $product, 'codes' => $codes, 'categories' => $categories, 'selectedCode' => 0, 'selectedCategory' => 0]);
    }

    public function create(ProductStoreRequest $request){
        $product = new Product;
        $product = $this->customFunctions->makeProductFromValidatedData($product, $request->validated());
        $product->save();
        return redirect()->route('products.index')->with('success', 'Nieuw product toegevoegd');
    }

    public function edit($id, ProductStoreRequest $request){
        $product = Product::find($id);
        $product = $this->customFunctions->makeProductFromValidatedData($product, $request->validated());
        $product->save();
        return redirect()->route('products.index')->with('success', 'Product gewijzigd');
    }
}
