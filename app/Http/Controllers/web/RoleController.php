<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Http\Requests\RoleStoreRequest;
use App\CustomFunctions;

class RoleController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Role $role){
        $roles = $role->sortable()->paginate(10);
        return view('roles', ['roles' => $roles]);
    }

    public function create(RoleStoreRequest $request){
        $role = new Role;
        $role = $this->customFunctions->makeRoleFromValidatedData($role, $request->validated());
        $role->save();
        return redirect()->route('roles.index')->with('success', 'Nieuwe rol toegevoegd');
    }

    public function destroy($id){
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('roles.index')->with('success', 'Rol verwijderd');
    }

    public function form($id){
        
        $role = Role::find($id);
        return view('forms.rolesform',['role' => $role]);
    }

    public function newform(){    
        return view('forms.rolesform');
    }


    public function edit($id, RoleStoreRequest $request){
        $role = Role::find($id);
        $role = $this->customFunctions->makeRoleFromValidatedData($role, $request->validated());
        $role->save();
        return redirect()->route('roles.index')->with('success', 'Rol gewijzigd');
    }


}
