<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Code;
use App\Http\Requests\CodeStoreRequest;
use App\CustomFunctions;

class CodeController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Code $code){
        $codes = $code->sortable()->paginate(10);
        return view('codes', ['codes' => $codes]);
    }

    public function create(CodeStoreRequest $request){
        $code = new Code;
        $code = $this->customFunctions->makeObjectFromValidatedData($code, $request->validated());
        $code->save();
        return redirect()->route('codes.index')->with('success', 'Nieuwe code toegevoegd');
    }

    public function destroy($id){
        $code = Code::find($id);
        $code->delete();
        return redirect()->route('codes.index')->with('success', 'Code verwijderd');
    }

    public function form($id){
        
        $code = Code::find($id);
        return view('forms.codesform',['code' => $code]);
    }

    public function newform(){    
        return view('forms.codesform');
    }


    public function edit($id, CodeStoreRequest $request){
        $code = Code::find($id);
        $code = $this->customFunctions->makeObjectFromValidatedData($code, $request->validated());
        $code->save();
        return redirect()->route('codes.index')->with('success', 'Code gewijzigd');
    }

}
