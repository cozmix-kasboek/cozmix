<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Person;
use App\Models\Role;
use App\Http\Requests\PersonStoreRequest;
use App\CustomFunctions;

class PersonController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Person $person){
        $persons = $person->sortable()->paginate(10);                    
        return view('persons')->with('persons', $persons);
    }

    public function destroy($id){
        $person = Person::find($id);
        $person->delete();
        return redirect()->route('persons.index')->with('success', 'Medewerker verwijderd');
    }

    public function form($id){
        
        $person = Person::find($id);
        $roles = Role::all();
        $selectedRole = $person->role_id;
        return view('forms.personsform',['person' => $person, 'roles' => $roles, 'selectedRole' => $selectedRole]);
    }

    public function newform(){   
        $roles = Role::all();     
        return view('forms.personsform', ['roles' => $roles, 'selectedRole' => 0]);
    }

    public function create(PersonStoreRequest $request){
        $person = new Person;
        $person = $this->customFunctions->makePersonFromValidatedData($person, $request->validated());
        $person->save();
        return redirect()->route('persons.index')->with('success', 'Nieuwe medewerker toegevoegd');
    }

    public function edit($id, PersonStoreRequest $request){
        $person = Person::find($id);
        $person = $this->customFunctions->makePersonFromValidatedData($person, $request->validated());
        $person->save();
        return redirect()->route('persons.index')->with('success', 'Medewerker gewijzigd');
    }
}