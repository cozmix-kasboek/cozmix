<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Person;
use App\Models\Type;
use App\Models\Event;
use App\Models\PaymentMethod;
use App\Models\ProductTransaction;
use App\Models\Product;
use App\Http\Requests\TransactionStoreRequest;
use App\CustomFunctions;

class TransactionController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Transaction $transaction){
        $transactions = $transaction->sortable()->paginate(10);   
        return view('transactions')->with('transactions', $transactions);
    }

    public function destroy($id){
        $transaction = Transaction::find($id);
        $transaction->delete();
        return redirect()->route('transactions.index')->with('success', 'Transactie verwijderd');
    }

    public function form($id){
        $transaction = Transaction::find($id);
        $selectedUser = $transaction->person_id;
        $selectedEvent = $transaction->event_id;
        $selectedType = $transaction->type_id;
        $selectedPaymentmethod = $transaction->payment_method_id;

        $linkedProducts = ProductTransaction::where('transaction_id',$id)->get();
        
        $persons = Person::all();
        $events = Event::all();
        $types = Type::all();
        $paymentmethods = PaymentMethod::all();
        $allProducts = Product::all();

        return view('forms.transactionsform', ['transaction' => $transaction, 'selectedUser' => $selectedUser, 'selectedEvent' => $selectedEvent, 'selectedType' => $selectedType, 'selectedPaymentmethod' => $selectedPaymentmethod, 'persons' => $persons, 'types' => $types, 'paymentmethods' => $paymentmethods, 'events' => $events, 'linkedProducts' => $linkedProducts, 'allProducts' => $allProducts]);


    }


    public function newform(){
        $transaction = new Transaction;
        $persons = Person::all();
        $events = Event::all();
        $types = Type::all();
        $paymentmethods = PaymentMethod::all();
        $allProducts = Product::all();

        return view('forms.transactionsform', ['transaction' => $transaction, 'selectedUser' => 0, 'selectedEvent' => "", 'selectedType' => 0, 'selectedPaymentmethod' => 0, 'persons' => $persons, 'types' => $types, 'paymentmethods' => $paymentmethods, 'events' => $events, 'linkedProducts' => 0, 'allProducts' => $allProducts]);


    }

    public function edit($id, TransactionStoreRequest $request){
        $transaction = Transaction::find($id);
        $transaction = $this->customFunctions->makeTransactionFromValidatedData($transaction, $request->validated());
        $transaction->save();
        return redirect()->route('transactions.index')->with('success', 'Transactie gewijzigd');
    }

    public function create(TransactionStoreRequest $request){
        $transaction = new Transaction;
        $transaction = $this->customFunctions->makeTransactionFromValidatedData($transaction, $request->validated());
        $transaction->save();
        return redirect()->route('transactions.index')->with('success', 'Nieuwe transactie toegevoegd');
    }

}
