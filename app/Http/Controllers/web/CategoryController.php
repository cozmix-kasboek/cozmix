<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Http\Requests\CategoryStoreRequest;
use App\CustomFunctions;

class CategoryController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(ProductCategory $productCategory){
        $categories = $productCategory->sortable()->paginate(10);
        return view('categories', ['categories' => $categories]);
    }

    public function create(CategoryStoreRequest $request){
        $category = new ProductCategory;
        $category = $this->customFunctions->makeObjectFromValidatedData($category, $request->validated());
        $category->save();
        return redirect()->route('categories.index')->with('success', 'Nieuwe categorie toegevoegd');
    }

    public function destroy($id){
        $product = ProductCategory::find($id);
        $product->delete();
        return redirect()->route('categories.index')->with('success', 'Product verwijderd');
    }

    public function form($id){
        
        $category = ProductCategory::find($id);
        return view('forms.categoriesform',['category' => $category]);
    }

    public function newform(){    
        return view('forms.categoriesform');
    }


    public function edit($id, CategoryStoreRequest $request){
        $product = ProductCategory::find($id);
        $product = $this->customFunctions->makeObjectFromValidatedData($product, $request->validated());
        $product->save();
        return redirect()->route('categories.index')->with('success', 'Categorie gewijzigd');
    }

}
