<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\CustomFunctions;
use App\Models\Type;
use App\Http\Requests\TypeStoreRequest;

class TransactionTypeController extends Controller
{
    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Type $type){
        $types = $type->sortable()->paginate(10);                  
        return view('types')->with('types', $types);
    }

    public function destroy($id){
        $type = Type::find($id);
        $type->delete();
        return redirect()->route('types.index')->with('success', 'Transactietype verwijderd');
    }

    public function form($id){        
        $type = Type::find($id);
        return view('forms.typesform',['type' => $type]);
    }

    public function newform(){    
        return view('forms.typesform');
    }

    public function create(TypeStoreRequest $request){
        $type = new Type;
        $type = $this->customFunctions->makeObjectFromValidatedData($type, $request->validated());
        $type->save();
        return redirect()->route('types.index')->with('success', 'Nieuw transactietype toegevoegd');
    }

    public function edit($id, TypeStoreRequest $request){
        $type = Type::find($id);
        $type = $this->customFunctions->makeObjectFromValidatedData($type, $request->validated());
        $type->save();
        return redirect()->route('types.index')->with('success', 'Transactietype gewijzigd');
    }
}
