<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\CustomFunctions;
use App\Models\PaymentMethod;
use App\Http\Requests\PaymentMethodStoreRequest;

class PaymentMethodController extends Controller
{
    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(PaymentMethod $paymentmethod){
        $paymentmethods = $paymentmethod->sortable()->paginate(10);                        
        return view('paymentmethods')->with('paymentmethods', $paymentmethods);
    }

    public function destroy($id){
        $paymentmethod = PaymentMethod::find($id);
        $paymentmethod->delete();
        return redirect()->route('paymentmethods.index')->with('success', 'Betaalmethode verwijderd');
    }

    public function form($id){        
        $paymentmethod = PaymentMethod::find($id);
        return view('forms.paymentmethodsform',['paymentmethod' => $paymentmethod]);
    }

    public function newform(){    
        return view('forms.paymentmethodsform');
    }

    public function create(PaymentMethodStoreRequest $request){
        $paymentmethod = new PaymentMethod;
        $paymentmethod = $this->customFunctions->makeObjectFromValidatedData($paymentmethod, $request->validated());
        $paymentmethod->save();
        return redirect()->route('paymentmethods.index')->with('success', 'Nieuwe transactietype toegevoegd');
    }

    public function edit($id, PaymentMethodStoreRequest $request){
        $paymentmethod = PaymentMethod::find($id);
        $paymentmethod = $this->customFunctions->makeObjectFromValidatedData($paymentmethod, $request->validated());
        $paymentmethod->save();
        return redirect()->route('paymentmethods.index')->with('success', 'Betaalmethode gewijzigd');
    }
}
