<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\CustomFunctions;
use App\Http\Requests\EventStoreRequest;

class EventController extends Controller
{

    private $customFunctions;

    public function __construct()
    {
        $this->customFunctions = new CustomFunctions();
    }

    public function index(Event $event)
    {
        $events = $event->sortable()->paginate(10);
        foreach ($events as $event) {
            $tickets_sold = 0;
            foreach ($event->transaction as $singleTransaction) {
                foreach ($singleTransaction->products as $product) {
                    $tickets_sold = $tickets_sold + $product->pivot->quantity;
                }
            }
            $event->tickets_sold = $tickets_sold;
        }
        return view('events', ['events' => $events]);
    }

    public function create(EventStoreRequest $request)
    {
        $event = new Event;
        $event = $this->customFunctions->makeEventFromValidatedData($event, $request->validated());
        $event->save();
        return redirect()->route('events.index')->with('success', 'Nieuwe gebeurtenis toegevoegd');
    }

    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        return redirect()->route('events.index')->with('success', 'Gebeurtenis verwijderd');
    }

    // public function destroy($id){
    //     $code = Code::find($id);
    //     $code->delete();
    //     return redirect()->route('codes.index')->with('success', 'Code verwijderd');
    // }

    public function form($id)
    {

        $event = Event::find($id);

        $tickets_sold = 0;

        foreach ($event->transaction as $singleTransaction) {
            $singleTransaction->quantity = 0;
            foreach ($singleTransaction->products as $product) {
                $tickets_sold = $tickets_sold + $product->pivot->quantity;
                $singleTransaction->quantity = $singleTransaction->quantity + $product->pivot->quantity;
            }
        }
        $event->tickets_sold = $tickets_sold;

        
        return view('forms.eventsform', ['event' => $event]);
    }

    public function newform()
    {
        return view('forms.eventsform');
    }


    public function edit($id, EventStoreRequest $request)
    {
        $event = Event::find($id);

        $event = $this->customFunctions->makeEventFromValidatedData($event, $request->validated());
        $event->save();

        return redirect()->route('events.index')->with('success', 'Gebeurtenis gewijzigd');
    }
}
