<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Person;
use App\CustomFunctions;

class UserController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(){
        $persons = Person::all();
        return response() -> json(['data' => $persons], 200);
    }

    public function show($id){
        $person = Person::findOrFail($id);
        return response() -> json(['data' => $person], 200);
    }

}
