<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Product;
use Illuminate\Http\Request;
use App\CustomFunctions;
use App\Http\Requests\MultipleTransactionStoreRequest;



class TransactionController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(){
        $transactions = Transaction::with(['person','type','event','paymentmethod','products.category'])->get();
        return response() -> json(['data' => $transactions], 200);
    }

    public function store(MultipleTransactionStoreRequest $request){

        $validatedTransactions = $request->validated()['transactions'];
        $savedTransactions = [];
        foreach($validatedTransactions as $transactionData){
            $transaction = new Transaction;
            $transaction = $this->customFunctions->makeApiTransactionFromValidatedData($transaction, $transactionData);
            array_push($savedTransactions, $transaction);
        }
        return response() -> json(['data' => $savedTransactions], 201);
    }

    public function show($id){
        $transaction = Transaction::with(['person','type','event','payment_method','products.category'])->where('id', $id)->get();
        return response() -> json(['data' => $transaction], 200);
    }

    public function destroy($id){
        $transaction = Transaction::find($id);
        $transaction ->delete();
        return response() -> json(['data' => $transaction], 200);
    }

    public function price(Request $request){
        $price = 0;

        $products = $request->products;

        foreach($products as $product){
            $productType = Product::find($product['product_id']);
            $price = $price + ($productType->price * $product['quantity']);
        }

        return response() -> json(['price' => $price], 200);
    }
}