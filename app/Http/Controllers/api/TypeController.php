<?php
namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Type;

class TypeController extends Controller
{
    public function index(){
        $types = Type::all();
        return response() -> json(['data' => $types], 200);
    }

    public function show($id){
        $type = Type::findOrFail($id);
        return response() -> json(['data' => $type], 200);
    }
}
