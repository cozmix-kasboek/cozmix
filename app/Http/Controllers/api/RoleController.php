<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\CustomFunctions;



class RoleController extends Controller
{
    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(){
        $roles = Role::all();
        return response() -> json(['data' => $roles], 200);
    }

    public function show($id){
        $role = Role::findOrFail($id);
        return response() -> json(['data' => $role], 200);
    }

    public function login(Request $request, $role){
        $pin = $request->pin;
        $role = Role::where(["name" => $role])->first();
        $pinCorrect = $pin == $role->pin;
        return response() -> json(['accepted' => $pinCorrect], 200);
    }
}
