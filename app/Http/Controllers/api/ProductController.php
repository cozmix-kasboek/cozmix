<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Product;


class ProductController extends Controller
{

    public function index(){
        $products = Product::all();
        return response() -> json(['data' => $products], 200);
    }

    public function show($id){
        $product = Product::findOrFail($id);
        return response() -> json(['data' => $product], 200);
    }
}
