<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\CustomFunctions;
use App\Http\Requests\MultipleEventStoreRequest;

class EventController extends Controller
{

    private $customFunctions;

    public function __construct(){
        $this->customFunctions = new CustomFunctions();
    }

    public function index(){
        $events = Event::all();
        foreach ($events as $event){
            $tickets_sold = 0;
            foreach($event->transaction as $singleTransaction){
                foreach($singleTransaction->products as $product){
                    $tickets_sold = $tickets_sold + $product->pivot->quantity;
                }
            }
            $event->tickets_sold = $tickets_sold;
        }
        return response() -> json(['data' => $events], 200);
    }

    public function store(MultipleEventStoreRequest $request){

        $savedEvents = [];

        $validatedEvents = $request->validated()['events'];

        foreach($validatedEvents as $eventData){
            $event = new Event;
            $event = $this->customFunctions->makeApiEventFromValidatedData($event, $eventData);
            $event->save();
            array_push($savedEvents, $event);
        }
        return response() -> json(['data' => $savedEvents], 201);
    }

    public function show($id){
        $event = Event::findOrFail($id);
        return response() -> json(['data' => $event], 200);
    }
}
