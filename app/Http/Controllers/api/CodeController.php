<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Code;

class CodeController extends Controller
{

    public function index(){
        $codes = Code::all();
        return response() -> json(['data' => $codes], 200);
    }

    public function show($id){
        $code = Code::findOrFail($id);
        return response() -> json(['data' => $code], 200);
    }
}
