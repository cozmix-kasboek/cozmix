<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;


class PaymentMethodController extends Controller
{
    public function index(){
        $codes = PaymentMethod::all();
        return response() -> json(['data' => $codes], 200);
    }

    public function show($id){
        $paymentmethod = PaymentMethod::findOrFail($id);
        return response() -> json(['data' => $paymentmethod], 200);
    }
}
