<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;

class CategoryController extends Controller
{
    public function index(){
        $categories = ProductCategory::all();
        return response() -> json(['data' => $categories], 200);
    }

    public function show($id){
        $category = ProductCategory::findOrFail($id);
        return response() -> json(['data' => $category], 200);
    }
}
