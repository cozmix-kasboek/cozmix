<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'name' => 'required|max:255',
            'datetime' => 'required|date_format:Y-m-d\TH:i',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'De productnaam ontbreekt',
            'name.max' => 'Productnaam maximum 255 karakters',
            'datetime.required' => 'Datum en tijdstip ontbreken',
            'datetime.date_format' => 'Datum en tijdstip in verkeerd formaat'
        ];
    }
}
