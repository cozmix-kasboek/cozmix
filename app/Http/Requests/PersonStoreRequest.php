<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'role_id' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'De voornaam ontbreekt',
            'last_name.required' => 'De familienaam ontbreekt',
            'role_id.required' => 'De rol ontbreekt',
            'first_name.max' => 'Voornaam maximum 255 karakters',
            'last_name.max' => 'Familienaam maximum 255 karakters'
        ];
    }
}
