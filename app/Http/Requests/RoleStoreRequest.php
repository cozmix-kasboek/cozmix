<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'nullable|max:255',
            'pin' => 'nullable|digits:4|integer'
        ];
    }

    
    public function messages()
    {
        return [
            'name.required' => 'De rolnaam ontbreekt',
            'name.max' => 'Rolnaam maximum 255 karakters',
            'description.max' => 'Omschrijving maximum 255 karakters',
            'pin.digits' => 'De pincode moet 4 cijfers lang zijn',
            'pin.integer' => 'De pincode moet een cijfer zijn'
        ];
    }
}
