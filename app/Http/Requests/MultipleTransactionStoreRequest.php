<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MultipleTransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'transactions.*.transaction.date' => 'required|date_format:Y-m-d',
            'transactions.*.transaction.description' => 'nullable|max:512',
            'transactions.*.transaction.type_id' => 'required|integer',
            'transactions.*.transaction.event_id' => 'nullable|uuid',
            'transactions.*.transaction.user_id' => 'integer',
            'transactions.*.transaction.transaction_amount' => 'nullable|regex:/^-?[0-9]\d*(\.\d+)?$/',
            'transactions.*.transaction.payment_method_id' => 'nullable|integer',
            'transactions.*.products.*.productId' => 'required|integer',
            'transactions.*.products.*.quantity' => 'required|integer'

        ];


        return $rules;
    }
}
