<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CodeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:5',
            'description' => 'nullable|max:255'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'De code ontbreekt',
            'name.max' => 'Code maximum 5 karakters',
            'description.max' => 'Omschrijving maximum 255 karakters',
        ];
    }
}
