<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

   
        return [
            'date' => 'required|date_format:Y-m-d',
            'description' => 'nullable|max:255',
            'type_id' => 'required|integer',
            'event_id' => 'nullable|uuid',
            'person_id' => 'required|integer',
            'payment_method_id' => 'nullable|integer',
            'transaction_amount' => 'nullable|regex:/^-?[0-9]\d*(\.\d+)?$/',
            'products.*' => 'nullable|integer',
            'quantities.*' => 'nullable|integer|min:1'

        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Datum ontbreekt',
            'date.date_format' => 'Datum in verkeerd formaat',
            'description.max' => 'Beschrijving maximum 255 karakters',
            'type_id.required' => 'Type ontbreekt',
            'person_id.required' => 'Medewerker ontbreekt',
            'transaction_amount.regex' => 'Totaalbedrag heeft verkeerd formaat',
            'quantities.*.min' => 'Aantal producten minimum 1'
        ];
    }
}
