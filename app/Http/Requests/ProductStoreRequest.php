<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'price' => 'nullable|min:0|regex:/^\d+(\.\d{1,2})?$/',
            'description' => 'nullable|max:255',
            'code_id' => 'required|integer',
            'category_id' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'De productnaam ontbreekt',
            'name.max' => 'Productnaam maximum 255 karakters',
            'price.min' => 'Prijs is negatief',
            'price.regex' => 'Prijs is fout formaat (juiste formaten: 11 of 11.05 of 11.00)',
            'description.max' => 'Omschrijving maximum 255 karakters',
            'code_id.required' => 'De boekhoudkundige code ontbreekt',
            'category_id.required' => 'De categorie ontbreekt'
        ];
    }
}
