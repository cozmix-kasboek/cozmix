<?php

namespace App\Exports;

use App\Transaction;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TransactionsByDateExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    protected $from_date;
    protected $to_date;

    function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    public function query(){
        $data = DB::table('transactions')
        ->leftJoin('people', 'transactions.person_id', '=', 'people.id')
        ->leftJoin('types', 'transactions.type_id', '=', 'types.id')
        ->leftJoin('events', 'transactions.event_id', '=', 'events.id')
        ->leftJoin('payment_methods', 'transactions.payment_method_id', '=', 'payment_methods.id')
        ->leftJoin('products_transactions', 'transactions.id', '=', 'products_transactions.transaction_id')
        ->leftJoin('products', 'products_transactions.product_id', '=', 'products.id')
        ->leftJoin('codes', 'products.code_id', '=', 'codes.id')
        ->whereBetween('date', [$this->from_date, $this->to_date])
        ->select('transactions.id AS transaction_id', 'transactions.date AS transaction_date', 'types.name AS type_name', 'people.first_name AS person_first_name', 'transactions.description AS transaction_description', 'events.name AS event_name','payment_methods.name AS payment_method_name',  'products.name AS product_name', 'products_transactions.quantity AS product_transaction_quantity',  'products.price')

        ->addSelect(DB::raw('products.price * products_transactions.quantity as rijtotaal'))
        ->addSelect(DB::raw('transactions.transaction_amount as transaction_amount'))
        ->addSelect(DB::raw('codes.name as code_name'))
        ->orderBy('transactions.id');

        return $data;
    }

        /**
     * @return array
     */

    public function headings(): array
    {
        return [
            'ID',
            'Transactiedatum',
            'Transactietype',
            'Medewerker',
            'Omschrijving',
            'Gebeurtenis',  
            'Betaalmethode',
            'Product',
            'Aantal',
            'Productprijs',
            'Rijtotaal',
            'Totaalbedrag transactie',
            'Code'
        ];
    }
}
