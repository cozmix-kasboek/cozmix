<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'id' => '123e4567-e89b-12d3-a456-426614174000',
            'name' => 'Groepsbezoek',
            'datetime' => '2020-12-01 15:00:00'
        ]);
        DB::table('events')->insert([
            'id' => '123e4567-e89b-12d3-a456-426614174002',
            'name' => 'Vaste voorstelling',
            'datetime' => '2020-12-01 16:00:00'
        ]);
    }
}
