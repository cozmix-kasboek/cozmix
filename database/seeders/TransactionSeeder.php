<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insert([
            'date' => '2020-10-29',
            'transaction_amount' => '5',
            'person_id' => '1',
            'type_id' => '1',
            'event_id' => '123e4567-e89b-12d3-a456-426614174000',
            'payment_method_id' => '1'
        ]);
    }
}
