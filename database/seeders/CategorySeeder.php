<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            'name' => 'Tickets',
        ]);
        DB::table('product_categories')->insert([
            'name' => 'Gadgets',
        ]);
        DB::table('product_categories')->insert([
            'name' => 'Onkosten',
        ]);
        DB::table('product_categories')->insert([
            'name' => 'Diverse inkomsten',
        ]);
        DB::table('product_categories')->insert([
            'name' => 'Lidgelden'
        ]);
    }
}
