<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name' => 'Ticketverkoop',
        ]);
        DB::table('types')->insert([
            'name' => 'Gadgetverkoop',
        ]);
        DB::table('types')->insert([
            'name' => 'Kas<->bank',
        ]);
        DB::table('types')->insert([
            'name' => 'Divers',
        ]);
        DB::table('types')->insert([
            'name' => 'Lidgeld',
        ]);
        DB::table('types')->insert([
            'name' => 'Onkosten',
        ]);
    }
}
