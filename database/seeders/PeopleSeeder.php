<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'first_name' => 'Stefanie',
            'last_name' => 'Ninclaus',
            'img_src' => 'stefanie_ninclaus.jpg',
            'role_id' => '2'
        ]);
        DB::table('people')->insert([
            'first_name' => 'Frank',
            'last_name' => 'Tamsin',
            'img_src' => 'frank_tamsin.jpg',
            'role_id' => '2'
        ]);
        DB::table('people')->insert([
            'first_name' => 'Arne',
            'last_name' => 'Dierickx',
            'img_src' => 'arne_dierickx.jpg',
            'role_id' => '2'
        ]);
    }
}
