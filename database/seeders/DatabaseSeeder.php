<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            CodeSeeder::class,
            EventSeeder::class,
            PaymentMethodSeeder::class,
            ProductSeeder::class,
            RoleSeeder::class,
            PeopleSeeder::class,
            TypeSeeder::class,
            TransactionSeeder::class,
            ProductTransactionSeeder::class,
        ]);
    }
}
