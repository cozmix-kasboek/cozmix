<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_transactions')->insert([
            'transaction_id' => '1',
            'product_id' => '1',
            'quantity' => '1'
        ]);
    }
}
