<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Ticket aan 5 euro',
            'price' => '5.00',
            'code_id' => '18',
            'category_id' =>'1',
        ]);

        DB::table('products')->insert([
            'name' => 'Ticket aan 6 euro',
            'price' => '6.00',
            'code_id' => '18',
            'category_id' =>'1',
        ]);

        DB::table('products')->insert([
            'name' => 'Space 4 Kids 7 euro',
            'price' => '7.00',
            'code_id' => '18',
            'category_id' =>'1',
        ]);

        DB::table('products')->insert([
            'name' => 'Space 4 Kids 8 euro',
            'price' => '8.00',
            'code_id' => '18',
            'category_id' =>'1',
        ]);

        DB::table('products')->insert([
            'name' => 'Gratis ticket',
            'price' => '0.00',
            'code_id' => '18',
            'category_id' =>'1',
        ]);

        DB::table('products')->insert([
            'name' => 'Glow Space Zonnestelsel',
            'price' => '7.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Glow Space Aarde',
            'price' => '5.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Glow Space Maan',
            'price' => '5.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Glow Space Sterren 3D',
            'price' => '5.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Mini Lab Ruimte',
            'price' => '12.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);
        
        DB::table('products')->insert([
            'name' => '3D Puzzel Space Exploration',
            'price' => '15.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Larsen Puzzel De Maan',
            'price' => '9.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'In de Ruimte - Detectivepuzzel',
            'price' => '9.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Het zonnestelsel',
            'price' => '10.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Expeditie Natuur 50 Sterrenbeelden & Planeten',
            'price' => '10.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Hemelkalender lopend en volgend jaar',
            'price' => '12.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'VVS Sterrenbeeldatlas',
            'price' => '7.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Draaibare sterrenkaart',
            'price' => '8.75',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Brochure planetenpad',
            'price' => '3.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'De oerknal',
            'price' => '9.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'De sterren',
            'price' => '6.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'De planeten',
            'price' => '5.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Waarnemen',
            'price' => '6.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Twaalf maanden sterrenkijken',
            'price' => '19.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Astronomische gids voor België',
            'price' => '29.95',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Boek Armand Pien',
            'price' => '15.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Paraplu met sterrenhemel',
            'price' => '18.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Poster (Andromeda - Plejaden)',
            'price' => '5.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Sterrenkaart - Maankaart',
            'price' => '5.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Eclipsbril',
            'price' => '2.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Comet',
            'price' => '36.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Illuminated Polaris',
            'price' => '63.50',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Black Hole',
            'price' => '40.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Earth',
            'price' => '32.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Jupiter',
            'price' => '36.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Mars',
            'price' => '32.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Neptune',
            'price' => '32.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Pluto & Charon',
            'price' => '40.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Saturn',
            'price' => '36.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Sun',
            'price' => '40.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Uranus',
            'price' => '32.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Venus',
            'price' => '32.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        
        DB::table('products')->insert([
            'name' => 'Celestial Buddies - Moon',
            'price' => '32.00',
            'code_id' => '19',
            'category_id' =>'2',
        ]);

        DB::table('products')->insert([
            'name' => 'Vervoerskosten',
            'price' => '0.00',
            'code_id' => '13',
            'category_id' => '3'
        ]);

        DB::table('products')->insert([
            'name' => 'Lidgeld',
            'price' => '0.00',
            'code_id' => '18',
            'category_id' => '5'
        ]);

        DB::table('products')->insert([
            'name' => 'Printpapier',
            'price' => '0.00',
            'code_id' => '4',
            'category_id' => '3'
        ]);

        DB::table('products')->insert([
            'name' => 'Wisselgeld',
            'price' => '0.00',
            'code_id' => '21',
            'category_id' => '4'
        ]);

        

    }
}
