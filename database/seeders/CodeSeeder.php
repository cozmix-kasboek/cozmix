<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('codes')->insert([
            'name' => '64140',
            'description' => 'Klein materiaal'
        ]);
        DB::table('codes')->insert([
            'name' => '64150',
            'description' => 'Onderhoud materiaal'
        ]);
        DB::table('codes')->insert([
            'name' => '64160',
            'description' => 'Schoonmaak'
        ]);
        DB::table('codes')->insert([
            'name' => '64200',
            'description' => 'Kantoorbenodigheden'
        ]);
        DB::table('codes')->insert([
            'name' => '64210',
            'description' => 'IT en website'
        ]);
        DB::table('codes')->insert([
            'name' => '64240',
            'description' => 'Algemeen drukwerk'
        ]);
        DB::table('codes')->insert([
            'name' => '64250',
            'description' => 'Verzendingskosten'
        ]);
        DB::table('codes')->insert([
            'name' => '64280',
            'description' => 'Andere kosten'
        ]);
        DB::table('codes')->insert([
            'name' => '64411',
            'description' => 'Lidgelden'
        ]);
        DB::table('codes')->insert([
            'name' => '64420',
            'description' => 'Representatiekosten'
        ]);
        DB::table('codes')->insert([
            'name' => '64430',
            'description' => 'Opleidingen en sociale activiteiten'
        ]);
        DB::table('codes')->insert([
            'name' => '64510',
            'description' => 'Manifestaties'
        ]);
        DB::table('codes')->insert([
            'name' => '64520',
            'description' => 'Verplaatsingskosten'
        ]);
        DB::table('codes')->insert([
            'name' => '64822',
            'description' => 'Aankoop documentatie, hulp- en leermiddelen'
        ]);
        DB::table('codes')->insert([
            'name' => '64824',
            'description' => 'Aankoop boeken'
        ]);
        DB::table('codes')->insert([
            'name' => '64825',
            'description' => 'Tijdschriften'
        ]);
        DB::table('codes')->insert([
            'name' => '64940',
            'description' => 'Publiciteit'
        ]);
        DB::table('codes')->insert([
            'name' => '70100',
            'description' => 'Lidgelden'
        ]);

        DB::table('codes')->insert([
            'name' => '74815',
            'description' => 'Verkoop cursussen, publicaties en brochures'
        ]);

        
        DB::table('codes')->insert([
            'name' => '74980',
            'description' => 'Inkomgelden (bezoekers)'
        ]);

        DB::table('codes')->insert([
            'name' => '76900',
            'description' => 'Uitzonderlijke opbrengsten'
        ]);
    }
}
