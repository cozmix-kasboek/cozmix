@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')

@if (session('success'))
<div class="flex justify-center mt-10">
    <span class="bg-green-400 font-bold text-white p-2 m-4 inline text-sm">
        {{ session('success') }}
    </span>
</div>
@endif



<div class="flex justify-center">
    <div class="flex justify-evenly w-3/5 mt-5 mb-5 p-2">
        <a class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2" href="/transacties/toevoegen">Transactie toevoegen</a>
        <a class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2" href="/transactietypes">Transactietypes beheren</a>
        <a class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2" href="/betaalmethodes">Betaalmethodes beheren</a>
    </div>
</div>


<div class="flex justify-center mb-10">
    <div class="shadow rounded-lg">
        <table class="min-w-full leading-normal">

            <tr>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('id', 'ID')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('date', 'Datum')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('time', 'Tijdstip aanmaak/wijziging')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('description', 'Description')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('transaction_amount', 'Bedrag')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('person.first_name', 'Medewerker')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('event.name', 'Gebeurtenis')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('paymentmethod.name', 'Betaalmethode')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">Acties</th>
            </tr>


            @foreach ($transactions as $transaction)
            <tr>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->id ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->date ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->time ?? null }}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->description ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->transaction_amount ?? null }}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->person->first_name ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->event->name ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $transaction->paymentmethod->name ?? null}}</td>
                <td class="flex px-5 py-5 border-b border-gray-200 bg-white text-sm"> <a class="btn bg-gray-200 mr-5 p-1" href="{{ url('transacties', ['id' => $transaction->id]) }}">Wijzigen</a>
                    <form action="{{ url('transacties', ['id' => $transaction->id]) }}" method="post">|
                        <input class="btn bg-red-200 ml-5 p-1 cursor-pointer" type="submit" value="Verwijderen" />
                        @method('delete')
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach

        </table>
        <div class="flex justify-center w-full p-2 mt-5">
            <a class="relative inline-flex items-center px-4 py-2 mr-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{ $transactions->withQueryString()->url(1) }}">Eerste pagina</a>

            <div class="inline-block w-auto">
                {{ $transactions->withQueryString()->links() }}
            </div>

            <a class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{ $transactions->url( $transactions->lastPage()) }}">Laatste pagina</a>



        </div>
        <div class="flex justify-center w-full p-2">
            <span class="mr-2">{{ $transactions->total() }} resultaten</span>
            <span class="ml-2 font-bold">Pagina {{ $transactions->currentPage() }} van {{ $transactions->lastPage() }}</span>
        </div>
    </div>
</div>



@endsection