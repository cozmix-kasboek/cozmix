<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cozmix</title>
    
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script src="https://use.fontawesome.com/1ea72c347e.js"></script>
</head>
<header class="grid grid-cols-3 bg-orange-300 font-bold text-orange-600"><h1 class="text-3xl p-8 tracking-wider"><a href="/">COZMIX BEHEERPLATFORM</a></h1>

<nav class="flex justify-center items-end col-span-2">
    <ul class="flex self-center text-lg">
        <li class="m-4 inline tracking-wider"><a href="/gebruikers">Gebruikers</a></li>
        <li class="m-4 inline tracking-wider"><a href="/producten">Producten</a></li>
        <li class="m-4 inline tracking-wider"><a href="/transacties">Transacties</a></li>
        <li class="m-4 inline tracking-wider"><a href="/gebeurtenissen">Gebeurtenissen</a></li>
        <li class="m-4 inline tracking-wider"><a href="/exports">Exporteren van data</a></li>
    </ul>
</nav>

</header>
<body>
        @section('sidebar')

        @show


            @yield('content')

</body>
</html>