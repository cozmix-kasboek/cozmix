@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent

@endsection

@section('content')

@if (session('success'))
<div class="text-green-500 font-bold">
    {{ session('success') }}
</div>
@endif


<div class="flex justify-center mt-10">

    <form class="w-4/12" method="post" action="/export">

        <div id="quickSelections" class="flex flex-wrap">
            <button class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2 mr-5 mb-2 quickSelectionButton" type="button" id="last7days">Laatste 7 dagen (incl. vandaag)</button>
            <button class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2 mr-5 mb-2 quickSelectionButton" type="button" id="last14days">Laatste 14 dagen (incl. vandaag)</button>
            <button class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2 mr-5 mb-2 quickSelectionButton" type="button" id="last30days">Laatste 30 dagen (incl. vandaag)</button>
            <button class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2 mr-5 mb-2 quickSelectionButton" type="button" id="yesterday">Gisteren</button>
            <button class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2 mr-5 mb-2 quickSelectionButton"type="button" id="sunday">Afgelopen zondag</button>
            <button class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2 mr-5 mb-2 quickSelectionButton"type="button" id="today">Vandaag</button>
        </div>

        <div class="mb-5">
            <label for="from">Van</label>
            <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="from" name="from_date" type="date" required>
        </div>

        <div class="mb-5">
            <label for="to">Tot</label>
            <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="to" name="to_date" type="date" required>
        </div>
        <div class="mb-5">
            <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Klik hier om de een lijst van transacties te downloaden">
        </div>
        @csrf
    </form>
</div>

@endsection

<script>
    document.addEventListener("DOMContentLoaded", init);

    function init() {
        console.log("DOM loaded");
        document.querySelectorAll(".quickSelectionButton").forEach(btn => {
            btn.addEventListener("click", processClick);
        })
    }

    function processClick(e){
        let action = e.target.id;
        switch(action) {
            case "last7days":
                returnLastXDays(6);
                break;
            case "last14days":
                returnLastXDays(13);
                break;
            case "last30days":
                returnLastXDays(29);
                break;
            case "yesterday":
                returnYesterday();
                break;
            case "sunday":
                returnSunday();
                break;
            case "today":
                returnToday();
                break;
            default:
                return;
        }
    }

    function subtractDays(days){
        let dateObject = new Date();
        dateObject.setDate(dateObject.getDate()-days);
        dateObject = dateObject.toISOString().slice(0,10);
        return dateObject;
    }

    function returnLastXDays(days){
        writeDatesToFormFields(subtractDays(days), subtractDays(0));
    }

    function returnYesterday(){
        writeDatesToFormFields(subtractDays(1));
    }

    function returnToday(){
        writeDatesToFormFields(subtractDays(0));
    }

    function returnSunday(){
        let dateObject = new Date();
        dateObject.setDate(dateObject.getDate() - (dateObject.getDay() || 7))
        dateObject = dateObject.toISOString().slice(0,10);
        writeDatesToFormFields(dateObject);
    }

    function writeDatesToFormFields(fromDate, toDate = fromDate){
        document.querySelector("#from").value = fromDate;
        document.querySelector("#to").value = toDate;
    }
</script>