@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')

@if ($errors->any())
<div class="flex justify-center mt-5">
    <ul class="flex flex-wrap inline">
        @foreach ($errors->all() as $error)
        <li class="bg-red-500 font-bold text-white p-2 m-4 inline text-sm">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


@if($transaction->exists)
<h1 class="text-center mt-10 text-lg font-bold">Transactie wijzigen</h1>
@method('put')
@else
<h1 class="text-center mt-10 text-lg font-bold">Transactie toevoegen</h1>
@endif



<div class="flex justify-center mt-10">

    @if($transaction->exists)
    <form class="w-4/12" method="post" action="{{ url('transacties', ['id' => $transaction->id]) }}">
        @method('put')
        @else
        <form class="w-4/12" method="post" action="/transacties/toevoegen">
            @endif
            <div class="mb-5">
                <label>Datum<span class="text-orange-500">*</span></label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="date" value="{{ old('date', $transaction->date) }}" name="date" required>
            </div>

            <div class="mb-5">
                <label>Omschrijving</label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="textarea" value="{{ old('description', $transaction->description) }}" name="description">
            </div>

            <div class="mb-5">
                <label>Medewerker<span class="text-orange-500">*</span></label>
                <select class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="persons" name="person_id" required>
                    <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($persons as $person)

                    @if ($person->id == $selectedUser || old('person_id') == $person->id)
                    <option value="{{ $person->id}}" selected>{{ $person->first_name }}</option>
                    @else
                    <option value="{{ $person->id }}">{{ $person->first_name}}</option>
                    @endif



                    @endforeach
                </select>
            </div>

            <div class="mb-5">
                <label>Transactietype<span class="text-orange-500">*</span></label>
                <select class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="text" value="{{ $paymentmethod->name ?? '' }}" id="types" name="type_id" required>
                    <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($types as $type)

                    @if ($type->id == $selectedType || old('type_id') == $type->id)
                    <option value="{{ $type->id}}" selected>{{ $type->name }}</option>
                    @else
                    <option value="{{ $type->id }}">{{ $type->name}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="mb-5">
                <label>Gekoppelde gebeurtenis</label>
                <select class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="events" name="event_id">
                    <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($events as $event)

                    @if ($event->id == $selectedEvent || old('event_id') == $event->id)
                    <option value="{{ $event->id}}" selected>{{ $event->name }} | {{date('d/m/Y', strtotime($event->datetime)) }} | {{date('H:i', strtotime($event->datetime)) }}</option>
                    @else
                    <option value="{{ $event->id }}">{{ $event->name }} | {{date('d/m/Y', strtotime($event->datetime)) }} | {{date('H:i', strtotime($event->datetime)) }}</option>
                    @endif
                    @endforeach

                </select>
                <button class="reset underline" type="button">Reset waarde</button>
            </div>

            <div class="mb-5">
                <label>Betaalmethode</label>
                <select class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="payment_methods" name="payment_method_id">
                    <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($paymentmethods as $paymentmethod)
                    @if ($paymentmethod->id == $selectedPaymentmethod || old('payment_method_id') == $paymentmethod->id)
                    <option value="{{ $paymentmethod->id}}" selected>{{ $paymentmethod->name }}</option>
                    @else
                    <option value="{{ $paymentmethod->id }}">{{ $paymentmethod->name}}</option>
                    @endif
                    @endforeach
                </select>
                <button class="reset underline" type="button">Reset waarde</button>
            </div>

            <div class="mb-5">
                <label>Producten</label>
                <button class="italic" type=button id="addProduct">
                    (Productregel toevoegen: <span class="underline">klik hier</span>)
                </button>
                <div class="flex flex-wrap w-full" id="products">
                    @if(old('products'))
                    @for ($i = 0; $i < count(old('products')); $i++) <div>
                        <select class="border-2 border-orange-500 border-opacity-75 p-1 mr-1 mb-1" name='products[]'>
                            <option hidden disabled selected value> -- Maak een keuze -- </option>
                            @foreach($allProducts as $key => $value)
                            <option value="{{ $value->id }}" {{ old('products.'.$i) == $value->id ? 'selected':'' }}>
                                {{ $value->name }}
                            </option>
                            @endforeach
                        </select>

                        <input class="border-2 border-orange-500 border-opacity-75 w-16 p-1 mb-1" name='quantities[]' type="number" value="{{ old('quantities.'.$i) }}" min="1">
                        <button class="underline delete mb-5">Verwijderen</div>
                </div>
                @endfor
                @elseif($transaction->exists)
                @foreach($transaction->products as $product)
                <div>
                    <select class="border-2 border-orange-500 border-opacity-75 p-1 mr-1 mb-1" name='products[]'>
                        @foreach($allProducts as $singleProduct)
                        <option value="{{ $singleProduct->id }}" {{ $product->id == $singleProduct->id ? 'selected':'' }}>{{ $singleProduct->name }}</option>
                        @endforeach
                    </select>

                    <input class="border-2 border-orange-500 border-opacity-75 w-16 p-1" name='quantities[]' type="number" value="{{ $product->pivot->quantity}}" min="1">
                    <button class="underline delete mb-5">Verwijderen</div>
                @endforeach
                @endif
            </div>
</div>


<input class="border-2 border-orange-500 border-opacity-75 w-32 p-1 mb-1 mr-5" name="transaction_amount" id="priceField" value="{{ old('transaction_amount', $transaction->transaction_amount)}}" readonly>

<input type="checkbox" name="fixed_price" id="fixedPrice" {{ old('fixed_price') == 'on' ? 'checked' : '' }}>
<label for="true">Prijs zelf bepalen (kostennota, lidgeld)</label>
<p class="font-bold">Totale bedrag van de transactie: <span id="totalPrice"> {{ old('transaction_amount', $transaction->transaction_amount) ?? '0' }} euro</span></p>

<select id="productStore" hidden>
    <option hidden disabled selected value> -- Maak een keuze -- </option>
    @foreach($allProducts as $product)
    <option value="{{ $product->id }}">
        {{ $product->name }}
    </option>
    @endforeach
</select>


<div class="mt-5 mb-5">
    <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Opslaan" />
</div>
@csrf


</form>



@endsection

<script>
    let opts = [];
    document.addEventListener("DOMContentLoaded", init);


    function init() {
        opts = document.querySelector("#productStore").options;
        let btn = document.querySelector('#addProduct');

        btn.addEventListener('click', createProductRow);
        document.querySelector('#products').addEventListener('change', calculatePrice);


        document.querySelectorAll(".delete").forEach(button => {
            button.addEventListener("click", deleteProduct);
        });

        document.querySelectorAll(".reset").forEach(button => {
            button.addEventListener("click", resetField);
        });

        document.querySelectorAll(".delete").forEach(button => {
            button.addEventListener("click", deleteProduct);
        });
        checkAndChangeSettingPriceField();
        document.querySelector('#fixedPrice').addEventListener('change', checkAndChangeSettingPriceField);
        document.querySelector("#priceField").addEventListener('change', writePrice);

    }

    function checkAndChangeSettingPriceField(){
        if (document.querySelector('#fixedPrice').checked){
            document.querySelector('#priceField').readOnly = false;
        } else {
            document.querySelector('#priceField').readOnly = true;
        }
    }

    function writePrice(){

        let price = document.querySelector('#priceField').value;
        document.querySelector("#totalPrice").innerHTML = price;


    }

    function createProductRow() {
        let products = document.querySelector("#products");

        let selectList = document.createElement("select");
        selectList.classList.add('border-2', 'border-orange-500', 'border-opacity-75', 'p-1', 'mr-1', 'mb-1');
        selectList.name = "products[]"


        for (let i = 0; i < opts.length; i++) {
            let option = document.createElement("option");
            option.value = opts[i].value;
            option.text = opts[i].innerHTML;
            selectList.append(option);
        }

        let inputField = document.createElement("input");
        inputField.name = "quantities[]";
        inputField.min = 1;
        inputField.value = 1;
        inputField.type = 'number';
        inputField.classList.add('border-2', 'border-orange-500', 'border-opacity-75', 'w-16', 'p-1', 'mb-1', 'mr-4');

        let deleteButton = document.createElement("button");
        deleteButton.classList.add('delete', 'btn');

        deleteButton.innerHTML = 'Verwijderen';

        let divElement = document.createElement("div");
        divElement.appendChild(selectList);
        divElement.appendChild(inputField);
        divElement.appendChild(deleteButton);
        products.append(divElement);
        document.querySelectorAll(".delete").forEach(button => {
            button.addEventListener("click", deleteProduct);
        });


    }

    function deleteProduct(e) {
        e.target.parentElement.remove();
        calculatePrice();
    }

    function resetField(e) {
        let fieldToReset = e.target.parentElement.children[1];
        fieldToReset.selectedIndex = 0;
    }

    function calculatePrice() {

        let checkbox = document.querySelector('#fixedPrice').checked;

        console.log(checkbox);

        if (checkbox){
            console.log("checkbox checked");
        }

        let obj = {
            'products': []
        };
        let selectFields = document.querySelectorAll('#products select:not([style="display:none"])');
        let inputFields = document.querySelectorAll('#products input:not([style="display:none"])');

        for (let i = 0; i < selectFields.length; i++) {
            obj.products.push({
                'product_id': parseInt(selectFields[i].value),
                'quantity': parseInt(inputFields[i].value)
            });
        }

        let bool = true;
        obj.products.forEach(o => {
            console.log(o.product_id);
            if (isNaN(o.product_id)) {
                console.log(o.product_id);
                bool = !bool;
                return;
            }
        });

        if (bool && !checkbox) {
            fetch('http://cozmix.local/api/transactions/price', {
                    method: 'POST', // or 'PUT'
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(obj),
                })
                .then(response => response.json())
                .then(data => {
                    document.querySelector('#totalPrice').innerHTML = `${parseFloat(data.price)} euro`;
                    document.querySelector('#priceField').value = parseFloat(data.price);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        }

    }
</script>