@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')


@if ($errors->any())
<div class="flex justify-center mt-5">
    <ul class="flex flex-wrap inline">
        @foreach ($errors->all() as $error)
        <li class="bg-red-500 font-bold text-white p-2 m-4 inline text-sm">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (isset($category))
<h1 class="text-center mt-10 text-lg font-bold">Categoriedetails wijzigen</h1>
@else
<h1 class="text-center mt-10 text-lg font-bold">Categorie toevoegen</h1>
@endif

<div class="flex justify-center mt-10">
    @isset($category)
    <form class="w-4/12" method="post" action="{{ url('categorieen', ['id' => $category->id]) }}">
        @method('put')
        @endisset
        <form class="w-4/12" method="post" action="/categorieen/toevoegen">
        <div class="mb-5">
            <label class="block">Naam<span class="text-orange-500">*</span></label>
            <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="text" value="{{ old('name', $category->name ?? '') }}" name="name" required>
            </div>
            <div class="mb-5">
            <label class="block">Omschrijving</label>
            <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="textarea" value="{{ old('description', $category->description ?? '') }}" name="description">
            </div>

            <div class="mb-5">
                <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Opslaan" />
            </div>

            @csrf


        </form>



        @endsection