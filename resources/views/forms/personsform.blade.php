@extends('layouts.master')

@section('title', 'Gebruikers')

@section('sidebar')
@parent


@endsection

@section('content')


@if ($errors->any())
<div class="flex justify-center mt-5">
    <ul class="flex flex-wrap inline">
        @foreach ($errors->all() as $error)
        <li class="bg-red-500 font-bold text-white p-2 m-4 inline text-sm">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (isset($person))
<h1 class="text-center mt-10 text-lg font-bold">Gebruikersdetails wijzigen</h1>
@else
<h1 class="text-center mt-10 text-lg font-bold">Gebruiker toevoegen</h1>
@endif

<div class="flex justify-center mt-10">



    @isset($person)


    <form class="w-4/12" method="post" action="{{ url('gebruikers', ['id' => $person->id]) }}">
        @method('put')
        @endisset


        <form class="w-4/12" method="post" action="/gebruikers/toevoegen">
            <div class="mb-5">
                <label class="block" for="first_name">Voornaam<span class="text-orange-500">*</span></label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="first_name" type="text" value="{{ old('first_name', $person->first_name ?? '') }}" name="first_name" required>
            </div>
            <div class="mb-5">
                <label class="block" for="last_name">Familienaam<span class="text-orange-500">*</span></label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="last_name" type="text" value="{{ old('last_name', $person->last_name ?? '') }}" name="last_name" required>
            </div>
            <div class="mb-5">

                <label class="block" for="role">Rol<span class="text-orange-500">*</span></label>

                <select class="block border-2 border-orange-500 border-opacity-75 w-full p-1" id="role" id="roles" name="role_id" required>
                    <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($roles as $role)
                    <option value="{{ $role->id}}" {{ $role->id == old('role_id', $person->role->id ?? '') ? 'selected' : ''}}>{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-5">
                <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Opslaan" />
            </div>

            @csrf


        </form>
</div>


@endsection