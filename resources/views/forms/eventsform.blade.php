@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')


@if ($errors->any())
<div class="flex justify-center mt-5">
    <ul class="flex flex-wrap inline">
        @foreach ($errors->all() as $error)
        <li class="bg-red-500 font-bold text-white p-2 m-4 inline text-sm">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


@if (isset($event))
<h1 class="text-center mt-10 text-lg font-bold">Gebeurtenis wijzigen</h1>
@else
<h1 class="text-center mt-10 text-lg font-bold">Gebeurtenis toevoegen</h1>
@endif

<div class="flex justify-center mt-10">
    @isset($event)
    <form class="w-4/12" method="post" action="{{ url('gebeurtenissen', ['id' => $event->id]) }}">
        @method('put')
        @endisset
        <form class="w-4/12" method="post" action="/gebeurtenissen/toevoegen">
            <div class="mb-5">
                <label>Naam<span class="text-orange-500">*</span></label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="text" value="{{ old('name', $event->name ?? '') }}" name="name"name="name" required >
            </div>
            <div class="mb-5">
                <label>Datum en tijdstip (uur van aanvang)<span class="text-orange-500">*</span></label>
                @if (isset($event))
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="datetime-local" value="{{ old('datetime', date('Y-m-d\TH:i', strtotime($event->datetime))) }}" name="datetime" required>

                @else
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="datetime-local" value="{{ old('datetime', '') }}" name="datetime" required>
                @endif

            </div>

            <div class="mb-5">
                <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Opslaan" />
            </div>


            @csrf


        </form>




</div>


@if (isset($event))
<h1 class="text-center mt-10 text-lg font-bold">Gekoppelde transacties</h1>
<p class="text-center mt-10 font-bold">{{ $event ->tickets_sold }} verkochte tickets tijdens deze gebeurtenis</p>

<div class="flex justify-center mt-10">
<div class="flex flex-wrap justify-center w-5/12">

        @foreach($event->transaction as $transaction)
        <div class="bg-gray-100 flex items-start w-40 flex-wrap justify-center m-4 p-4 rounded-lg" >
            <p class="font-bold">{{ $transaction->date }} </p>
            <p class="mb-3"> {{ $transaction->quantity}} ticket(s)</p>
            <p><a class="underline" href="{{ url('transacties', ['id' => $transaction->id]) }}">Details bekijken</a></p>
        </div>
        @endforeach

</div>
</div>

@endif





@endsection