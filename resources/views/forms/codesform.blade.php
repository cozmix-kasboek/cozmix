@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')

@if ($errors->any())
<div class="flex justify-center mt-10">
    <ul class="flex flex-wrap inline">
        @foreach ($errors->all() as $error)
        <li class="bg-red-500 font-bold text-white p-2 m-4 inline text-sm">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (isset($code))
<h1 class="text-center mt-10 text-lg font-bold">Boekhoudkundige code wijzigen</h1>
@else
<h1 class="text-center mt-10 text-lg font-bold">Boekhoudkundige code toevoegen</h1>
@endif

<div class="flex justify-center mt-10">
    @isset($code)
    <form class="w-4/12" method="post" action="{{ url('codes', ['id' => $code->id]) }}">
        @method('put')
        @endisset
        <form class="w-4/12" method="post" action="/codes/toevoegen">
            <div class="mb-5">
                <label>Code<span class="text-orange-500">*</span></label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="text" value="{{ old('name', $code->name ?? '') }}" name="name" required>
            </div>
            <div class="mb-5">
                <label>Omschrijving</label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="textarea" value="{{ old('description', $code->description ?? '') }}" name="description">
            </div>

            <div class="mb-5">
                <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Opslaan" />
            </div>

            @csrf


        </form>
</div>


@endsection