@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent

@endsection

@section('content')

@if ($errors->any())
<div class="flex justify-center mt-5">
    <ul class="flex flex-wrap inline">
        @foreach ($errors->all() as $error)
        <li class="bg-red-500 font-bold text-white p-2 m-4 inline text-sm">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


@if ($product->exists))
<h1 class="text-center mt-10 text-lg font-bold">Productdetails wijzigen</h1>
@else
<h1 class="text-center mt-10 text-lg font-bold">Product toevoegen</h1>
@endif

<div class="flex justify-center mt-10">

    @if($product->exists)
    <form class="w-4/12" method="post" action="{{ url('producten', ['id' => $product->id]) }}">
    @method('put')
    @else
    <form class="w-4/12" method="post" action="/producten/toevoegen">
    @endif

            <div class="mb-5">
                <label class="block">Productnaam<span class="text-orange-500">*</span></label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="text" value="{{ old('name', $product->name ?? '') }}" name="name" required>
            </div>
            <div class="mb-5">
                <label class="block">Prijs</label>
                <input class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="text" value="{{ old('price', $product->price ?? '') }}" name="price">
            </div>

            <div class="mb-5">
                <label class="block">Omschrijving</label>
                <input  class="border-2 border-orange-500 border-opacity-75 w-full p-1" type="textarea" value="{{ old('description', $product->description ?? '') }}" name="description">
            </div>

            <div class="mb-5">
                <label class="block">Boekhoudkundige code<span class="text-orange-500">*</span></label>
                <select class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="codes" name="code_id" required>
                <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($codes as $code)

                    <option value="{{$code->id}}" {{ $code->id == old('code_id', $product->code->id ?? '') ? 'selected' : ''}}>
                        {{ $code->name }} | {{ $code -> description}}
                    </option> 
                    @endforeach
                </select>
            </div>

            <div class="mb-5">
                <label class="block">Categorie<span class="text-orange-500">*</span></label>
                <select class="border-2 border-orange-500 border-opacity-75 w-full p-1" id="categories" name="category_id" required>
                <option hidden disabled selected value> -- Maak een keuze -- </option>
                    @foreach($categories as $category)


                    <option value="{{$category->id}}" {{ $category->id == old('category_id', $product->category->id ?? '') ? 'selected' : ''}}>
                        {{ $category->name }}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="mb-5">
                <input class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider w-full border-b-2" type="submit" value="Opslaan" />
            </div>

            @csrf


        </form>
</div>


@endsection