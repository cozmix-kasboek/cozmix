@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
@parent

@endsection

@section('content')

@if (session('success'))
<div class="flex justify-center mt-10">
    <span class="bg-green-400 font-bold text-white p-2 m-4 inline text-sm">
    {{ session('success') }}
    </span>
</div>
@endif


<div class="flex justify-center">
    <div class="flex justify-evenly w-2/5 mt-5 mb-5 p-2">
        <a class="p-1 font-bold text-orange-600 uppercase rounded-lg text-xs tracking-wider border-b-2" href="/codes/toevoegen">Code toevoegen</a>
    </div>
</div>

<div class="flex justify-center">
    <div class="shadow rounded-lg">
        <table class="min-w-full leading-normal">
            <tr>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('id', 'ID')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('name', 'Code')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">@sortablelink('description', 'Omschrijving')</th>
                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">Acties</th>
            </tr>


            @foreach ($codes as $code)
            <tr>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $code->id ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $code->name ?? null}}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{ $code->description ?? null }}</td>
                <td class="flex px-5 py-5 border-b border-gray-200 bg-white text-sm"><a class="btn bg-gray-200 mr-5 p-1" href="{{ url('codes', ['id' => $code->id]) }}">Wijzigen</a>|<form action="{{ url('codes', ['id' => $code->id]) }}" method="post">
                        <input class="btn bg-red-200 ml-5 p-1 cursor-pointer" type="submit" value="Verwijderen" />
                        @method('delete')
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach

        </table>
        <div class="flex justify-center w-full p-2 mt-5">
            <a class="relative inline-flex items-center px-4 py-2 mr-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{ $codes->withQueryString()->url(1) }}">Eerste pagina</a>

            <div class="inline-block w-auto">
                {{ $codes->withQueryString()->links() }}
            </div>

            <a class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{ $codes->url( $codes->lastPage()) }}">Laatste pagina</a>



        </div>
        <div class="flex justify-center w-full p-2">
            <span class="mr-2">{{ $codes->total() }} resultaten</span>
            <span class="ml-2 font-bold">Pagina {{ $codes->currentPage() }} van {{ $codes->lastPage() }}</span>
        </div>

    </div>
</div>
@endsection